# Supersort

Python library that provides an efficient sorting function.

To use supersort simply add this folder to the same folder as a python program
and import it using the following:

> from supersort.supersort import sorted

Supersort can then be used by simply calling sorted.
